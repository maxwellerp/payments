# 51JK-POS接口
#### 0.[访问权限](https://gitee.com/jk51/erp-pos/blob/master/accessToken.md)
#### 1.[POS对51JK发送订单](https://gitee.com/jk51/erp-pos/blob/master/newOrder.md)
#### 2.[51JK发送新会员信息至POS](https://gitee.com/jk51/erp-pos/blob/master/memberInfo.md)
#### 3.[51JK对POS发送支付结果报告](https://gitee.com/jk51/erp-pos/blob/master/paymentNotice.md)
